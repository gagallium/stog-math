let wrap_latex str = [Xtmpl.E (("", "latex"), [], [ Xtmpl.D str ])]

let fun_math _env _args subs =
  let input = Xtmpl.string_of_xmls subs in
  wrap_latex ("\\[ " ^ input ^ " \\]")

let fun_inline_math _env _args subs =
  let input = Xtmpl.string_of_xmls subs in
  wrap_latex ("$" ^ input ^ "$")
  

(* use EQ as this is what 'pandoc --gladtex' produces out of inline
   $foo$...  the case-sensitivity of such elements names has been
   reported on the stog-devel mailing-list. *)
let () = Stog_plug.register_rule ("", "EQ") fun_inline_math;;
let () = Stog_plug.register_rule ("", "math") fun_math;;
