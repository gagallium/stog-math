MKDIR=mkdir -p
CP=cp -f

OCAMLC=ocamlc
OCAMLOPT=ocamlopt
OCAMLFIND=ocamlfind

PACKAGES=stog

INCLUDES=
COMPFLAGS=$(INCLUDES) -g -annot -package $(PACKAGES) -rectypes
LINKFLAGS=$(INCLUDES)
LINKFLAGS_BYTE=$(INCLUDES)

PLUGIN=stog_math.cmxs
PLUGIN_BYTE=$(PLUGIN:.cmxs=.cma)

all: byte opt
opt: $(PLUGIN)
byte: $(PLUGIN_BYTE)

stog_math.cmxs: stog_math.cmx
	$(OCAMLFIND) ocamlopt -linkpkg -shared -o $@ \
	$(LINKFLAGS) `ls $^ | grep -v cmi`

stog_math.cma: stog_math.cmo
	$(OCAMLFIND) ocamlc -a -linkpkg -o $@ \
	$(LINKFLAGS_BYTE) `ls $^ | grep -v cmi`

install:
	$(OCAMLFIND) install stog-math META \
	$(PLUGIN) $(PLUGIN_BYTE)

uninstall:
	$(OCAMLFIND) remove stog-math

distclean: clean

clean:
	rm -f *.cm* *.o *.annot


# Rules
.SUFFIXES: .mli .ml .cmi .cmo .cmx .mll .mly

%.cmi:%.mli
	$(OCAMLFIND) ocamlc $(COMPFLAGS) -c $<

%.cmo:%.ml
	if test -f `dirname $<`/`basename $< .ml`.mli && test ! -f `dirname $<`/`basename $< .ml`.cmi ; then \
	$(OCAMLFIND) ocamlc $(COMPFLAGS) -c `dirname $<`/`basename $< .ml`.mli; fi
	$(OCAMLFIND) ocamlc $(COMPFLAGS) -c $<

%.cmi %.cmo:%.ml
	if test -f `dirname $<`/`basename $< .ml`.mli && test ! -f `dirname $<`/`basename $< .ml`.cmi ; then \
	$(OCAMLFIND) ocamlc $(COMPFLAGS) -c `dirname $<`/`basename $< .ml`.mli; fi
	$(OCAMLFIND) ocamlc $(COMPFLAGS) -c $<

%.cmx %.o:%.ml
	$(OCAMLFIND) ocamlopt $(COMPFLAGS) -c $<

%.ml:%.mll
	$(OCAMLLEX) $<

.PHONY: clean depend

.depend depend:
	ocamldep *.ml > .depend


include .depend
